// console.log("Hello World");

/*
	1.  [function WITHOUT return]
		Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

    -invoke and pass 2 arguments to the addition function (test arguments: 5 and 15)
*/

    function add(firstNumber, secondNumber) {
        console.log(`Displayed sum of ${firstNumber} and ${secondNumber}`);
        console.log(firstNumber + secondNumber);
    }

    add(5, 15);

/*
		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

    -invoke and pass 2 arguments to the subtraction function (test arguments:  20 and 5)
*/

    function subtract(firstNumber, secondNumber) {
        console.log(`Displayed difference of ${firstNumber} and ${secondNumber}`);
        console.log(firstNumber - secondNumber);
    }

    subtract(20, 5);

/*
-------------------------------------------------
	2.  [function with return]	
		Create a function which will be able to multiply two numbers. (test arguments: 50 and 10) 
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

        Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.

        Log the value of product variable in the console.
*/

    function multiply(firstNumber, secondNumber) {
        console.log(`Displayed product of ${firstNumber} and ${secondNumber}`);
        return firstNumber * secondNumber;
    }

    let multiplication = multiply(50, 10);
    console.log(multiplication);

/*
    Create a function which will be able to divide two numbers. (test arguments: 50 and 10) 
        -Numbers must be provided as arguments.
        -Return the result of the division.

    Create a global variable called outside of the function called quotient.
        -This quotient variable should be able to receive and store the result of division function.

    Log the value of quotient variable in the console.
*/

    function divide(firstNumber, secondNumber) {
        console.log(`Displayed quotient of ${firstNumber} and ${secondNumber}`);
        return firstNumber / secondNumber;
    }

    let quotient = divide(50, 10);
    console.log(quotient);

/*
-------------------------------------------------
	3. [function with return]	
		Create a function which will be able to get total area of a circle from a radius number (not variable) as argument (test argument: 15) .
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive a result from an argument and store the result of the circle area calculation.

        Log the value of the circleArea variable in the console.
*/

    function computeCircleArea(radius) {
        let computeArea = Math.PI * radius**2;
        let roundOff = parseFloat(computeArea.toFixed(2));
        console.log(`The result of getting the area of a circle with ${radius}:`);
        return roundOff;
    }

    let circleArea = computeCircleArea(15);
    console.log(circleArea);

/*
-------------------------------------------------
	4. [function with return]	
		Create a function which will be able to get total average of four numbers. (test arguments: 20, 40, 60, and 80)
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.
        
        Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive arguments and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/

    function computeAverage(first, second, third, fourth) {
        totalAverage = (first + second + third + fourth) / 4;
        console.log(`The average of ${first}, ${second}, ${third}, and ${fourth}`)
        return totalAverage;
    }

    let averageVar = computeAverage(20, 40, 60, 80);
    console.log(averageVar);


/*
-------------------------------------------------
	5. [function with return]	
		Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

    function checkPass(score, total) {
        let isPassed = ((score / total) * 100) >= 75;
        console.log(`Is ${score}/${total} a passing score?`)
        return isPassed;
    }

    let isPassingScore = checkPass(38, 50);
    console.log(isPassingScore);

